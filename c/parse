/* Copyright 1997 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/************************************************************************/
/*                  Copyright 1996 Acorn Computers Ltd                  */
/*                                                                      */
/*  This material is the confidential trade secret and proprietary      */
/*  information of Acorn Computers. It may not be reproduced, used      */
/*  sold, or transferred to any third party without the prior written   */
/*  consent of Acorn Computers. All rights reserved.                    */
/*                                                                      */
/************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "constants.h"
#include "parse.h"

/*
 * compare two strings caselessly
 */
int
cstrcmp(char *a, char *b)
{
  int d;

  if (a==NULL && b==NULL)
    return 0;

  if (a==NULL || b==NULL)
    return (-1);

  while (*a || *b)
  {
    d = tolower( *(a++) ) - tolower( *(b++) );
    if (d) return d;
  }
  return 0;
}

/*
 * malloc an area for a string and copy the string in
 */
char *
strdup(char *str)
{
  char *newstr=malloc(str==NULL?1:(strlen(str)+1));

  if (newstr != NULL)
  {
    if (str==NULL)
      *newstr = 0;
    else
      strcpy(newstr, str);
  }
  return (newstr);
}

/*
 * read an environment variable in to a malloced string
 */
char *
read_env(char *variable)
{
  char *var=NULL;
  char *cp =NULL;

  if ((var=getenv(variable))!=NULL)
    cp=strdup(var);

  return (cp);
}

/*
 * identify newline in string and replace with 0
 */
void
chop_newline(char *str)
{
  while (*str!=0 && *str!='\n')
    str++;
  *str=0;
}

/*
 * return string in a malloced block, leave pointing at next character
 * trashes the string in the process
 */
char *
extract_word(char *str, char **word, int env_var)
{
  int len=0;
  char *cp;
  char *var=NULL;
  int end_of_string = FALSE;

  if (str==NULL)			/* deal with bogus input */
    return (NULL);

  while ((*str==' ') || (*str==9)) 	/* skip space chars */
    str++;

  cp=str;
  while ((*cp!=' ') && (*cp!=9) && (*cp!=0))
  {
    len++;
    cp++;
  }
  if (*cp==0)
    end_of_string=TRUE;
  else
    *cp=0;

  if (strlen(str)==0)
    *word = NULL;
  else
  {
    if (env_var)			/* check for environment variable */
    {
      if ((var=getenv(str))!=NULL)	/* exists */
        *word=strdup(var);
      else				/* does not exist */
        *word = strdup(str);
    }
    else
      *word = strdup(str);
  }

  return (end_of_string==FALSE)?(str+len+1):NULL;
}

/*
 * parse the command line to extract arguments in form argc and argv
 */
int
parse_getargs(char *line, char **argv)
{
  int  argc = 0;
  char *cp;

  cp = line;
  while (*cp != '\0')
  {
    while (*cp == ' ')
      cp++;
    if (*cp == '\0')
      break;

    argv[argc++] = cp;
    while (*cp != '\0' && *cp != ' ')
      cp++;
    if (*cp == 0)
      break;
    *cp++ = '\0';
  }
  *cp++ = '\0';

  return (argc);
}
